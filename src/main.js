import Vue from 'vue'
import App from './App.vue'
import router from './router'
import 'bootstrap/dist/css/bootstrap.css'
import SideBar from '@/layouts/sideBar.vue'
import BreadCrumb from '@/layouts/breadcrumb.vue'


Vue.component('sidebar', SideBar)
Vue.component('breadcrumb', BreadCrumb)

Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
