import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from '../layouts/login.vue'
import Productos from '../views/Productos.vue'
import ProductosDetalle from '../views/ProductosDetalle.vue'
import CrearProducto from '../views/CrearProducto.vue'
import EditarProducto from '../views/EditarProducto.vue'
import Admin from '../views/Admin.vue'
import CrearAdministrador from '../views/CrearAdministrador.vue'
import AdminDetalle from '../views/AdminDetalle.vue'
import RouterExample from '../views/RouterExample.vue'
import Configuracion from '../views/Configuracion.vue'
import Prueba from '../views/prueba.vue'



Vue.use(VueRouter)

const routes = [{
    path: '/',
    name: 'login',
    component: Login
  },
  {
    path: '/productos',
    name: 'productos',
    component: Productos
  },
  {
    path: '/admin',
    name: 'admin',
    component: Admin
  },
  {
    path: '/configuracion',
    name: 'configuracion',
    component: Configuracion
  },
  {
    path: '/ejemplo',
    name: 'ejemplo',
    component: RouterExample
  },
  {
    path: '/productos/crear',
    name: 'crearProducto',
    component: CrearProducto
  },
  {
    path: '/productos/editar',
    name: 'editarProducto',
    component: EditarProducto
  },
  {
    path: '/prueba',
    name: 'prueba',
    component: Prueba
  },
  {
    path: '/productos/detalle',
    name: 'producto-detalle',
    component: ProductosDetalle
  },
  {
    path:'/admin/crear',
    name: 'crearAdministrador',
    component: CrearAdministrador
  }, 
  {
    path:'/admin/detalle',
    name: 'adminDetalle',
    component: AdminDetalle
  }

]

const router = new VueRouter({
  //removemos el uso del hash en la url
  mode: 'history',
  routes
})

export default router