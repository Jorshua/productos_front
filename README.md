# productos_front

## Estado del proyecto en trello
Ir [Al tablero del proyecto](https://trello.com/invite/b/3tb23r1p/62102c3291aa21faedc0d3c49d97312e/produtcx).

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
